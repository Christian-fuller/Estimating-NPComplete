import static java.util.function.Function.identity;
import java.util.stream.*;
import java.nio.file.*;
import java.util.*;
import java.io.*;

public class Main {
	static HashMap<Integer, HashSet<Integer>> nodes;
	static HashMap<Integer, Integer> nodeColors;
	static int minNode;

	public static void main(String args[]) {
		Scanner in = new Scanner(System.in);
		nodes = new HashMap<>();
		nodeColors = new HashMap<>();
		minNode = Integer.MAX_VALUE;
		System.out.println("Enter path to graph data");
		String path = in.nextLine();
		populate(path);
		System.out.println("Enter path to coloring");
		String colorPath = in.nextLine();
		color(colorPath);
		check();
	}

	public static void populate(String path) {
		try {
			Stream<String> fileStream = Files.lines(Paths.get(path));
			int count = 0;
			for (String i: fileStream.toArray(String[]::new)) {
				if (count != 0) {
					String[] temp = i.split(",");
					var nodeNum = Integer.parseInt(temp[0]);
					var neighNum = Integer.parseInt(temp[1]);

					if (nodeNum < minNode) {
						minNode = nodeNum;
					}

					if (neighNum < minNode) {
						minNode = neighNum;
					}

					//Building the graph, this bit is buidling and edge from nodeNum->neighNum
					if (nodes.containsKey(nodeNum)) {
						nodes.get(nodeNum).add(neighNum);
					} else {
						HashSet<Integer> newSet = new HashSet<>();
						newSet.add(neighNum);
						nodes.put(nodeNum, newSet);
						nodeColors.put(nodeNum, -1);
					}

					//Edge from neighNum->nodeNum
					if (nodes.containsKey(neighNum)) {
						nodes.get(neighNum).add(nodeNum);
					} else {
						HashSet<Integer> newSet = new HashSet<>();
						newSet.add(nodeNum);
						nodes.put(neighNum, newSet);
						nodeColors.put(neighNum, -1);
					}
				}
				count++;
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public static void color(String colorPath) {
		try {
			Stream<String> fileStream = Files.lines(Paths.get(colorPath));
			int curNode = minNode;
			int count = 0;
			for (String i: fileStream.toArray(String[]::new)) {
				if (count != 0) {
					nodeColors.put(curNode, Integer.parseInt(i));
					curNode++;
				}
				count++;
			}

		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public static void check() {
		boolean bad = false;

		for (Map.Entry<Integer, HashSet<Integer>> i: nodes.entrySet()) {
			int node = i.getKey();
			int nodeCol = nodeColors.get(node);
			HashSet<Integer> curNeighs = i.getValue();
			HashSet<Integer> colorSet = new HashSet<>(); //set of colors of the neighbors

			//populating the color set of neighbors
			for (Integer j: curNeighs) {
				colorSet.add(nodeColors.get(j));
			}

			for (Integer j: curNeighs) {
				if (nodeColors.get(j) == nodeCol) {
					bad = true;
					System.out.println("Bad coloring:");
					System.out.println("Node " + node + " is the same color as " + j);
				}
			}
		}

		if (!bad) {
			System.out.println("You got yourself a proper coloring");		
		}
	}
}
